timestamp=$(date +%s)

tag=0.0.1
tag=${tag}-${timestamp}
image=happycloudpak/springboot-service
baseDir=/Users/juheon/workspace/git/mvp-jenkins-springboot-sample
baseDeployDir=/deployment-iks
appname=springboot-service
containername=springboot-service-container
namespace=default

echo tag=${tag}
echo image=${image}


docker build -f ${baseDir}${baseDeployDir}/Dockerfile -t ${image}:${tag} ${baseDir}
docker push ${image}:${tag}
docker tag ${image}:${tag} ${image}:latest
docker push ${image}:latest

# echo "kubectl set image deployment ${appname} ${containername}=${image}:${tag} -n ${namespace}"

# kubectl set image deployment ${appname} ${containername}=${image}:${tag} -n ${namespace}