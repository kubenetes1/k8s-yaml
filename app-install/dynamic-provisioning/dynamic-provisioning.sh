# helm을 이용하여 dynamic-nfs provisioning 설치 
# nfs.server 변경
# nfs.path 변경
helm repo update
helm install \ 
--set podSecurityPolicy.enabled=true \ 
--set nfs.server=169.56.170.179  \   # nfs server 주소로 변경 
--set nfs.path=/srv/nfs  \           # nfs server shared folder root 위치 
stable/nfs-client-provisioner \ 
--tls

# 생성 확인
kubectl get sc | grep nfs-client


# test pvc 생성 
# storageClassName: nfs-client 로 설정
# pvc 이름 생성 : 예) pvc-nfs
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs # pvc name
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs-client  # kubectl get storageclass 로 확인할 것 
  resources:
    requests:
      storage: 2Gi

# test deploement 생성 
# claimName: pvc-nfs 을 위에서 생성한 이름으로 변경
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-test
  labels:
    app: nfs-test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nfs-test
  template:
    metadata:
      labels:
        app: nfs-test
    spec:
      containers:
      - name: nfs-test
        image: busybox
        imagePullPolicy: IfNotPresent
        command: ["sh"]
        args: ["-c", "while true; do sleep 60; done"]
        volumeMounts:
          - name: pv-data
            mountPath: /data
      volumes:
        - name: pv-data
          persistentVolumeClaim:
            claimName: pvc-nfs # pvc name
